\documentclass{beamer}

\usepackage{listings}
\usepackage{slashbox}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{amsmath,amssymb}
\usepackage{hyperref}
\usepackage{graphicx}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\maximize}{maximize}
\DeclareMathOperator*{\minimize}{minimize}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\RR}{\mathbb R}
\newcommand{\NN}{\mathbb N}

% Set transparency of non-highlighted sections in the table of
% contents slide.
\setbeamertemplate{section in toc shaded}[default][100]
\AtBeginSection[]
{
  \setbeamercolor{section in toc}{fg=red} 
  \setbeamercolor{section in toc shaded}{fg=black} 
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\title{Interactive visualization and machine learning\\
my research results from 2013}
\author{
Toby Dylan Hocking
}

\date{24 January 2014}

\maketitle

\begin{frame}
  \frametitle{A brief history of Toby}
  \begin{description}
  \item[2006] Bachelor degrees in Molecular Cell Biology and Statistics, UC
    Berkeley. Honors thesis with Terry Speed.
  \item[2006-2008] biochemistry experiments and
    statistical web database development at Sangamo BioSciences.
  \item[2009] Masters of Statistics, Paris 6. Thesis on a Bayesian
    model of population genetics with Mathieu Gautier and Jean-Louis
    Foulley.
  \item[2012] Ph.D in machine learning applied to bioinformatics, \\
    Ecole
    Normale Sup\'erieure de Cachan, with Jean-Philippe Vert
    (Curie) and Francis Bach (INRIA).
  \item[2013] Post-doc in Masashi Sugiyama's machine
    learning lab at Tokyo Institute of Technology.
  \end{description}
\end{frame}

\section{Learning to compare, with Supaporn Spanurattana
  and Masashi Sugiyama from Tokyo Tech} 

\begin{frame}
  \frametitle{Motivating example: learning to compare sushi}
  Data source: \url{http://www.kamishima.net/sushi/}

  \includegraphics[width=1in]{sushi_salmon}\makebox[2.5in]{salmon is
    better than eel}\includegraphics[width=1in]{sushi_anago}

  \includegraphics[width=1in]{sushi_chu-toro}\makebox[2.5in]{\alert<2>{fatty
      tuna is as good as
      crab liver}}\includegraphics[width=1in]{sushi_kani-miso}

If I give you another sushi pair,\\
can you tell me which one is better,\\
\alert<2>{or if they are equally good?}
\end{frame}

\begin{frame}
  \frametitle{One training datum is a pair of labeled sushi}
  \begin{tabular}{ccc}
    salmon & 
    \makebox[1in]{
    \only<1>{is better than}
    \only<3>{is as good as}
    \only<2>{is worse than}
}
    & eel\\
  \includegraphics[width=1in]{sushi_salmon} & 
  &
  \includegraphics[width=1in]{sushi_anago}
% > sushi.features$items[c(salmon=15,anago=1)+1,]
%          style major minor      oily frequency.eat    price frequency.sold
% サーモン     1     0     1 1.2711235      2.045497 1.511774           0.64
% 穴子         1     0     3 0.9263844      1.990228 1.992459           0.88
    \\
    \underline{salmon feature vector} &
    \underline{label} & 
    \underline{eel feature vector}
\\
 $x = \left[
    \begin{array}{c}
1 \\
      1.27  \\
      1.51 
    \end{array}
    \right]$
    \begin{tabular}{c}
      style \\
      oily \\
      price
    \end{tabular} &
    $y = 
    \only<1>{-1}
    \only<3>{0}
    \only<2>{1}
$ &
  $x' = \left[
    \begin{array}{c}
      3 \\
      0.93  \\
      1.99 
    \end{array}
    \right]$
    \begin{tabular}{c}
      style \\
      oily \\
      price
    \end{tabular}
  \end{tabular}
\end{frame}

\begin{frame}
  \frametitle{Related problem: detecting differential expression}
  MYCN gene expression in
  \begin{tabular}{ccc}
    normal cells & 
    \makebox[1in]{
    \only<2>{is higher than}
    \only<3>{is the same as}
    \only<1>{is lower than}
}
    & in cancer cells\\
    \underline{MYCN normal} &
    \underline{label} & 
    \underline{MYCN cancer}
\\
 $x = \left[
    \begin{array}{c}
1 \\
      1.27  \\
      1.51 
    \end{array}
    \right]$
    \begin{tabular}{c}
      rep~1 \\
      rep~2 \\
      rep~3
    \end{tabular} &
    $y = 
    \only<2>{-1}
    \only<3>{0}
    \only<1>{1}
$ &
  $x' = \left[
    \begin{array}{c}
      3 \\
      0.93  \\
      1.99 
    \end{array}
    \right]$
    \begin{tabular}{c}
      rep~1 \\
      rep~2 \\
      rep~3
    \end{tabular}
  \end{tabular}
  \begin{itemize}
  \item Features are e.g. replicates in a microarray experiment.
  \item Predict up, down, or no change for every gene, e.g. MYCN.
  \item Positive control genes: $y\in\{-1,1\}$.
  \item Negative control genes: $y=0$.
  \item Controls can be used to train an algorithm,\\
    e.g. p-value threshold in paired 2-sample t-test.
  \item Proposed SVMcompare algorithm useful when you have\\
    control genes and incomparable/un-normalized features.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Learning a function to automatically label a pair}
  One training set is $n$ labeled pairs of sushi/genes \\
$
    \begin{array}{c}
  (\mathbf x_1,\mathbf x_1',y_1) \\
  (\mathbf x_2,\mathbf x_2',y_2) \\
  \vdots \\
  (\mathbf x_n,\mathbf x_n',y_n) 
    \end{array}
$
  \begin{itemize}
  \item Input: a pair of feature vectors $\mathbf x_i,\mathbf x_i'\in\RR^p$\\
    e.g. number of features $p=3$ style, oily, price.
  \item Output: a label $y_i=
  \begin{cases}
    -1 & \text{ if $\mathbf x_i$ is better}\\
    0 & \text{ if $\mathbf x_i$ is as good as $\mathbf x'_i$}\\
    1 & \text{ if $\mathbf x'_i$ is better}.
  \end{cases}
$
\item Goal: find a comparison function with good prediction
  $c(\mathbf x_i, \mathbf x_i')\in\{-1,0,1\}$ 
$$\minimize_c \sum_{i\in\text{test}} 
I\left[ y_i \neq c(\mathbf x_i,\mathbf x_i') \right],$$
where $I[\cdot]\in\{0, 1\}$ is the indicator function.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Related work: reject, rank, and rate}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|c|c|c|}\hline
  \backslashbox{Outputs}{Inputs}
  &single items $\mathbf x$&pairs of items $\mathbf x,\mathbf x'$\\ \hline
  $y\in\{-1,1\}$ &SVM  & SVMrank   	\\ \hline 
  $y\in\{-1,0,1\}$ &Reject option& \textbf{this work}\\ \hline
\end{tabular}
\begin{itemize}
\item SVM=Support Vector Machines, Cortes and Vapnik, 1995.
\item PL Bartlett and MH Wegkamp. Classification with a reject
  option using a hinge loss. JMLR, 9:1823--1840, 2008. (statistical
  properties of the hinge loss)
\item T Joachims. Optimizing search engines using clickthrough
  data. KDD 2002. (SVMrank)
\item K Zhou \emph{et al.} Learning to rank with ties. SIGIR
  2008. (boosting, ties are more effective with more output values)
\item R Herbrich \emph{et al.} TrueSkill: a Bayesian skill rating
  system. NIPS 2006. (generalization of Elo for chess)
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusions and future work}
  \begin{itemize}
  \item Proposed a new algorithm: SVMcompare.
  \item R package: \url{https://github.com/tdhock/rankSVMcompare}
  \item Learns % a nonlinear ranking function $r(\mathbf x)\in\RR$, and  \item
    a comparison function $c(\mathbf x, \mathbf x')\in\{-1,0,1\}$.
  \item Result: better prediction than the baseline SVMrank method.
  \item Submitted to AISTATS Iceland 22-25 April 2014.
  \end{itemize}
\end{frame}

\section{animint: an R package for interactive data visualization,
  with Google Summer of Code student Susan VanderPlas}

\begin{frame}
  \frametitle{animint produces interactive animations}
  \begin{itemize}
  \item Useful for high-dimensional time series data.
  \item \textbf{100s of lines} of code using standard GUI toolkits
    e.g. \texttt{library(tcltk)} in R or \texttt{import Tkinter} in
    Python.
  \item animint reduces this to \textbf{10s of lines} of R/ggplot2 code.\\
    (also less code than other recent R packages)
  \item Rendered in a web browser using the D3 Javascript library.\\
    Bostock et al 2011. D3 data-driven documents. IEEE Transactions on
    Visualization and Computer Graphics.
  \item Submitted tutorial on interactive graphics to useR July 2014,
    UCLA, discusses lots of related work.
  \item Submission to a data visualization conf in Paris, Nov 2014?
  \end{itemize}
  \url{http://github.com/tdhock/animint}
\end{frame}

\begin{frame}
  \frametitle{Google Summer of Code}
Student gets \$5000 for writing open source code for
    3 months.
    \begin{description}
    \item[March] Admins (such as myself) for open source organizations
      e.g. R, Bioconductor apply to Google.
    \item[April] Mentors suggest projects for each org.
    \item[May] Students submit project proposals to Google.
    \item Mentors rank student/project proposals.
    \item Google gives $n$ students to an org.
    \item[June]The top $n$ students get \$500 and begin coding.
    \item[August] Midterm evaluation, pass = \$2250.
    \item[September] Final evaluation, pass = \$2250.
    \item[November] Orgs get \$500/student mentored.
    \end{description}
    We can be \textbf{mentors} (get code written) and
    \textbf{students} (get paid).
\end{frame}

\section{SegAnnDB: interactive genomic data segmentation}

\begin{frame}
  \frametitle{Coauthors}
  \begin{description}
  \item[Institut Curie, Paris] Valentina Boeva, Guillem Rigaill, Gudrun
  Schleiermacher, Isabelle Janoueix-Lerosey, Olivier
  Delattre, Wilfrid Richer, Franck Bourdeaut, Jean-Philippe Vert.
  \item[INRIA/ENS, Paris] Francis Bach.
  \item[Aichi Cancer Center, Nagoya] Miyuki Suguro, Masao Seto.
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Computer vision: look and add labels to...}
  \begin{tabular}{ccc}
    Photos & Cell images & Copy number profiles \\
    \includegraphics[width=1.3in]{faces} &
    \includegraphics[width=1.3in]{cellprofiler} &
    \includegraphics[width=1.5in]{regions-axes}\\
    Labels: names & phenotypes & alterations
  \end{tabular}
  Sources: \url{http://en.wikipedia.org/wiki/Face_detection}\\
  Jones et al PNAS 2009. Scoring diverse cellular morphologies in
  image-based screens with iterative feedback and machine learning.
\end{frame}

\begin{frame}
  \frametitle{Computer vision for genomic segmentation}
  \begin{tabular}{ccc}
    Breakpoint\\
 detector & strong points & weak points \\
    \hline
    your eyes & outliers, signal/noise & finding the \\
    & over large regions &  exact breakpoint\\
    \hline
    mathematical  & maximum likelihood & tuning parameters \\
    models & algorithm finds exact  & chosen using\\
& breakpoint locations  & unrealisic assumptions
  \end{tabular}
\textbf{SegAnnDB exploits the strong points of both:}
\begin{itemize}
\item Plot a maximum likelihood model alongside the data.
\item Edit annotated regions.
\item The model parameters are automatically updated to agree.
\item No tuning parameters, but need to annotate a few profiles.
\end{itemize}
Demo on \url{http://bioviz.rocq.inria.fr/}
\end{frame}

\begin{frame}
  \frametitle{Annotated regions allow supervised and interactive models}
  \begin{tabular}{c|c|c|c}
    Input: data + & parameters & annotated regions & Plot/update?\\
    \hline
    & \textbf{unsupervised} & \textbf{supervised} & no\\
    & default & params chosen to \\
    & params & agree w/regions\\
    \hline
    & \textbf{tweaked} & \textbf{interactive} & yes\\
    & params & edit regions
  \end{tabular}
  \begin{description}
  \item[2004-now] dozens of \textbf{unsupervised} models that can be
    \textbf{tweaked} e.g. GLAD, DNAcopy, fused lasso, cghseg, PELT.
  \item[2009-2010] Janoueix-Lerosey, Schleiermacher, et al. J Clinical
    Oncology. Breakpoint region labels recorded by typing 0/1 in Excel
    spreadsheets.
  \item[2013] Hocking et al. BMC Bioinformatics protocols for training
    \textbf{supervised} models.
  \item[2014?] paper about SegAnnDB software for \textbf{interactive}
    genomic segmentation submitted to Bioinformatics.
  \item[2014?] McGill server, co-authored papers, citations from YOU?
    Nick ChIP data, Simon methylation data.
  \end{description}
\end{frame}

\begin{frame} 
  \frametitle{Advantages of supervised learning}
  Creating annotation databases is important for
  \begin{description}
  \item[short-term] quantitative evaluation, more convincing papers.
  \item[long-term] cross-discipline collaboration!
    \begin{description}
    \item[biologists/annotators] get
    better models.
    \item[statisticians/modelers] get better evaluation
    metrics.
    \end{description}
  \end{description}
  Can we make labels/supervised approaches for...
  \begin{description}
  \item[segmentation] breakpoint/copy number regions (this work).
  \item[clustering] pairs that should join (or not)?
  \item[regression] variables/genes that are important (or not)?
  \item[diff expression] genes that are up/down (or not)?
  \end{description}
  Write me at \texttt{toby.hocking@mail.mcgill.ca} to collaborate!
\end{frame}

\section{Bathroom reading group}

\begin{frame}
  \frametitle{Learn something in 30 seconds}
  Share technical knowledge with your colleagues,\\
  via 1-page bathroom poster, e.g.
  \begin{itemize}
    \item an interesting web site
    \item some useful software
    \item something interesting from a textbook
    \item a figure from a new article
  \end{itemize}
  Google and Facebook do it.
\end{frame}

\begin{frame}
  \frametitle{A useful \LaTeX/machine learning web site}
  \includegraphics[width=\textwidth]{detexify}
\end{frame}

\begin{frame}
  \frametitle{R packages for making plots}
  \includegraphics[width=\textwidth]{plots}
\end{frame}

\begin{frame}
  \frametitle{Textbook model for predicting survival times}
  \includegraphics[width=\textwidth]{aft}
\end{frame}

\begin{frame}
  \frametitle{Ideas?}
  \begin{itemize}
  \item I will start.
  \item Change every 2 weeks?
  \item which floors?
  \item subjects?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Thank you!}
  Supplementary slides appear after this one.
\end{frame}

\end{document}
